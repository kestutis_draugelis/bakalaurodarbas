## Interface: 60000
## Title: Light Unit Frames
## Notes: Test Add-on
## Author: Kestutis Draugelis
## Version: v0.1
## SavedVariables: LUFDB
## OptionalDeps: Ace3, LibSharedMedia-3.0, AceGUI-3.0-SharedMediaWidgets, LibDualSpec-1.0, Clique


#@no-lib-strip@
libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\LibSharedMedia-3.0\lib.xml
libs\AceDB-3.0\AceDB-3.0.xml
libs\LibDualSpec-1.0\LibDualSpec-1.0.lua
#@end-no-lib-strip@

LUF.lua
modules\units.lua
modules\layout.lua
modules\defaultlayout.lua
modules\health.lua
modules\power.lua

