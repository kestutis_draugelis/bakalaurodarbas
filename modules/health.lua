local Health = {}
LUF:RegisterModule(Health, "healthBar", LUF.L["Health bar"], true)

local function getGradientColor(unit)
	local maxHealth = UnitHealthMax(unit)
	local percent = maxHealth > 0 and UnitHealth(unit) / maxHealth or 0
	if( percent >= 1 ) then return LUF.db.profile.healthColors.green.r, LUF.db.profile.healthColors.green.g, LUF.db.profile.healthColors.green.b end
	if( percent == 0 ) then return LUF.db.profile.healthColors.red.r, LUF.db.profile.healthColors.red.g, LUF.db.profile.healthColors.red.b end
	
	local sR, sG, sB, eR, eG, eB = 0, 0, 0, 0, 0, 0
	local modifier, inverseModifier = percent * 2, 0
	if( percent > 0.50 ) then
		sR, sG, sB = LUF.db.profile.healthColors.green.r, LUF.db.profile.healthColors.green.g, LUF.db.profile.healthColors.green.b
		eR, eG, eB = LUF.db.profile.healthColors.yellow.r, LUF.db.profile.healthColors.yellow.g, LUF.db.profile.healthColors.yellow.b

		modifier = modifier - 1
	else
		sR, sG, sB = LUF.db.profile.healthColors.yellow.r, LUF.db.profile.healthColors.yellow.g, LUF.db.profile.healthColors.yellow.b
		eR, eG, eB = LUF.db.profile.healthColors.red.r, LUF.db.profile.healthColors.red.g, LUF.db.profile.healthColors.red.b
	end
	
	inverseModifier = 1 - modifier
	return eR * inverseModifier + sR * modifier, eG * inverseModifier + sG * modifier, eB * inverseModifier + sB * modifier
end

Health.getGradientColor = getGradientColor

function Health:OnEnable(frame)
	if( not frame.healthBar ) then
		frame.healthBar = LUF.Units:CreateBar(frame)
	end
	
	frame:RegisterUnitEvent("UNIT_HEALTH", self, "Update")
	frame:RegisterUnitEvent("UNIT_MAXHEALTH", self, "Update")
	frame:RegisterUnitEvent("UNIT_CONNECTION", self, "Update")
	frame:RegisterUnitEvent("UNIT_FACTION", self, "UpdateColor")
	frame:RegisterUnitEvent("UNIT_THREAT_SITUATION_UPDATE", self, "UpdateColor")
	frame:RegisterUnitEvent("UNIT_HEALTH_FREQUENT", self, "Update")
	frame:RegisterUnitEvent("UNIT_TARGETABLE_CHANGED", self, "UpdateColor")
	
	if( frame.unit == "pet" ) then
		frame:RegisterUnitEvent("UNIT_POWER", self, "UpdateColor")
	end
	
	frame:RegisterUpdateFunc(self, "UpdateColor")
	frame:RegisterUpdateFunc(self, "Update")
end

function Health:OnDisable(frame)
	frame:UnregisterAll(self)
end

function Health:UpdateColor(frame)
	frame.healthBar.hasReaction = nil
	frame.healthBar.hasPercent = nil
	frame.healthBar.wasOffline = nil
	
	local color
	local unit = frame.unit
	local reactionType = LUF.db.profile.units[frame.unitType].healthBar.reactionType
	if( not UnitIsConnected(unit) ) then
		frame.healthBar.wasOffline = true
		frame:SetBarColor("healthBar", LUF.db.profile.healthColors.offline.r, LUF.db.profile.healthColors.offline.g, LUF.db.profile.healthColors.offline.b)
		return
	elseif( LUF.db.profile.units[frame.unitType].healthBar.colorAggro and UnitThreatSituation(frame.unit) == 3 ) then
		frame:SetBarColor("healthBar", LUF.db.profile.healthColors.aggro.r, LUF.db.profile.healthColors.aggro.g, LUF.db.profile.healthColors.aggro.b)
		return
	elseif( frame.inVehicle ) then
		color = LUF.db.profile.classColors.VEHICLE
	elseif( LUF.db.profile.units[frame.unitType].healthBar.colorType == "static" ) then
		color = LUF.db.profile.healthColors.static
	end
	
	if( color ) then
		frame:SetBarColor("healthBar", color.r, color.g, color.b)
	else
		frame.healthBar.hasPercent = true
		frame:SetBarColor("healthBar", getGradientColor(unit))
	end
end

function Health:Update(frame)
	local isOffline = not UnitIsConnected(frame.unit)
	frame.isDead = UnitIsDeadOrGhost(frame.unit)
	frame.healthBar.currentHealth = UnitHealth(frame.unit)
	frame.healthBar:SetMinMaxValues(0, UnitHealthMax(frame.unit))
	frame.healthBar:SetValue(isOffline and UnitHealthMax(frame.unit) or frame.isDead and 0 or frame.healthBar.currentHealth)

end
