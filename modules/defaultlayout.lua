local L = LUF.L
local playerClass = select(2, UnitClass("player"))

local function finalizeData(config, useMerge)
	local self = LUF
	local function mergeToChild(parent, child, forceMerge)
		for key, value in pairs(parent) do
			if( type(child[key]) == "table" ) then
				mergeToChild(value, child[key], forceMerge)
			elseif( type(value) == "table" ) then
				child[key] = CopyTable(value)
			elseif( forceMerge or ( value ~= nil and child[key] == nil ) ) then
				child[key] = value
			end
		end
	end

	local function verifyTable(tbl, checkTable)
		for key, value in pairs(tbl) do
			if( type(value) == "table" ) then
				if( not checkTable[key] ) then
					tbl[key] = nil
				else
					for subKey, subValue in pairs(value) do
						if( type(subValue) == "table" ) then
							verifyTable(value, checkTable[key])
						end
					end
				end
			end
		end
	end
	
	for unit, child in pairs(config.units) do
		if( self.defaults.profile.units[unit] ) then
			if( not useMerge or ( useMerge and not self.db.profile.units[unit].enabled and self.db.profile.units[unit].height == 0 and self.db.profile.units[unit].width == 0 and self.db.profile.positions[unit].anchorPoint == "" and self.db.profile.positions[unit].point == "" ) ) then
				mergeToChild(config.parentUnit, child)
				verifyTable(child, self.defaults.profile.units[unit])
				mergeToChild(child, self.db.profile.units[unit], true)
				
				if( useMerge and self.db.profile.positions[unit].point == "" and self.db.profile.positions[unit].relativePoint == "" and self.db.profile.positions[unit].anchorPoint == "" and self.db.profile.positions[unit].x == 0 and self.db.profile.positions[unit].y == 0 ) then
					self.db.profile.positions[unit] = config.positions[unit]
				end
			end
		end
	end

	self.db.profile.loadedLayout = true
	
	if( not useMerge ) then
		config.parentUnit = nil
		config.units = nil
		
		for key, data in pairs(config) do
			self.db.profile[key] = data
		end
	else
		for key, data in pairs(config) do
			if( key ~= "parentUnit" and key ~= "units" and key ~= "positions" and key ~= "hidden" ) then
				if( self.db.profile[key] == nil ) then
					self.db.profile[key] = data
				else
					for subKey, subValue in pairs(data) do
						if( self.db.profile[key][subKey] == nil ) then
							self.db.profile[key][subKey] = subValue
						end
					end
				end
			end
		end
	end
end

function LUF:LoadDefaultLayout(useMerge)
	local config = {}
	config.bars = {
		texture = "Minimalist",
		spacing = -1.25,
		alpha = 1.0,
		backgroundAlpha = 0.20,
	}
	config.auras = {
		borderType = "light",
	}
	config.backdrop = {
		tileSize = 1,
		edgeSize = 5,
		clip = 1,
		inset = 3,
		backgroundTexture = "Chat Frame",
		backgroundColor = {r = 0, g = 0, b = 0, a = 0.80},
		borderTexture = "None",
		borderColor = {r = 0.30, g = 0.30, b = 0.50, a = 1},
	}
	config.hidden = {
		cast = false, buffs = false, party = true, player = true, pet = true, target = true, focus = true, boss = true, arena = true, playerAltPower = false, playerPower = true
	}
	config.font = {
		name = "Myriad Condensed Web",
		size = 11,
		extra = "",
		shadowColor = {r = 0, g = 0, b = 0, a = 1},
		color = {r = 1, g = 1, b = 1, a = 1},
		shadowX = 0.80,
		shadowY = -0.80,
	}
	local SML = LibStub:GetLibrary("LibSharedMedia-3.0")
	if( GetLocale() == "koKR" or GetLocale() == "zhCN" or GetLocale() == "zhTW" or GetLocale() == "ruRU" ) then
		config.font.name = SML.DefaultMedia.font
	end
	
	config.powerColors = {
		MANA = {r = 0.30, g = 0.50, b = 0.85}, 
		RAGE = {r = 0.90, g = 0.20, b = 0.30},
		FOCUS = {r = 1.0, g = 0.50, b = 0.25},
		ENERGY = {r = 1.0, g = 0.85, b = 0.10}, 
		
	}
	config.healthColors = {
		static = {r = 0.00, g = 0.00, b = 0.00},
	}
	config.positions = {

		target = {anchorPoint = "RC", anchorTo = "#LUFUnitplayer", x = 520, y = -100}, 
		player = {point = "TOPLEFT", anchorTo = "UIParent", relativePoint = "TOPLEFT", y = -100, x = -520}, 
	}
	
	config.parentUnit = {
		portrait = {enabled = false},
		text = {
			{width = 0.50, name = L["Left text"], anchorTo = "$healthBar", anchorPoint = "CLI", x = 3, y = 0, size = 0, default = true},
			{width = 0.60, name = L["Right text"], anchorTo = "$healthBar", anchorPoint = "CRI", x = -3, y = 0, size = 0, default = true},

			{width = 0.50, name = L["Left text"], anchorTo = "$powerBar", anchorPoint = "CLI", x = 3, y = 0, size = 0, default = true},
			{width = 0.60, name = L["Right text"], anchorTo = "$powerBar", anchorPoint = "CRI", x = -3, y = 0, size = 0, default = true},

			{width = 1, name = L["Text"], anchorTo = "$emptyBar", anchorPoint = "CLI", x = 3, y = 0, size = 0, default = true},
		},
		indicators = {
			raidTarget = {anchorTo = "$parent", anchorPoint = "C", size = 20, x = 0, y = 0},
			class = {anchorTo = "$parent", anchorPoint = "BL", size = 16, x = 0, y = 0},
			masterLoot = {anchorTo = "$parent", anchorPoint = "TL", size = 12, x = 16, y = -10},
			leader = {anchorTo = "$parent", anchorPoint = "TL", size = 14, x = 2, y = -12},
			pvp = {anchorTo = "$parent", anchorPoint = "TR", size = 22, x = 11, y = -21},
			ready = {anchorTo = "$parent", anchorPoint = "LC", size = 24, x = 35, y = 0},
			role = {anchorTo = "$parent", anchorPoint = "TL", size = 14, x = 30, y = -11},
			status = {anchorTo = "$parent", anchorPoint = "LB", size = 16, x = 12, y = -2},
			lfdRole = {enabled = false, anchorPoint = "BR", size = 14, x = 3, y = 14, anchorTo = "$parent"}
		},
		emptyBar = {background = false, height = 1, reactionType = "none", order = 0},
		powerBar = {background = true, height = 1.0, order = 20, colorType = "type"},
	}
	
	config.units = {
			width = 90,
			height = 30,
			scale = 0.85,
			unitsPerColumn = 8,
			maxColumns = 8,
			columnSpacing = 5,
			groupsPerRow = 8,
			groupSpacing = 0,
			attribPoint = "TOP",
			attribAnchorPoint = "LEFT",
			healthBar = {reactionType = "none"},
			powerBar = {height = 0.30},
			incHeal = {cap = 1},
			incAbsorb = {cap = 1},
			healAbsorb = {cap = 1},
			indicators = {
				pvp = {anchorTo = "$parent", anchorPoint = "BL", size = 22, x = 0, y = 11},
				masterLoot = {anchorTo = "$parent", anchorPoint = "TR", size = 12, x = -2, y = -10},
				role = {enabled = false, anchorTo = "$parent", anchorPoint = "BR", size = 14, x = 0, y = 14},
				ready = {anchorTo = "$parent", anchorPoint = "LC", size = 24, x = 25, y = 0},
			},
			text = {
				{text = "[name]"},
				{text = "[missinghp]"},
				{text = ""},
				{text = ""},
				{text = "[name]"},
			},
		},
		player = {
			width = 300,
			height = 80,
			scale = 1.0,
			portrait = {enabled = false, fullAfter = 50},
			comboPoints = {enabled = true, anchorTo = "$parent", order = 60, anchorPoint = "BR", x = -3, y = 8, size = 14, spacing = -4, growth = "LEFT", isBar = true, height = 0.40},
			indicators = {
                resurrect = {enabled = true, anchorPoint = "LC", size = 28, x = 37, y = -1, anchorTo = "$parent"},
			},
			auras = {
				buffs = {enabled = false, maxRows = 1},
				debuffs = {enabled = false, maxRows = 1},
			},
			text = {
				{text = "[name]"},
				{text = "[curmaxhp]"},
				{text = "[perpp]"},
				{text = "[curmaxpp]"},
				{text = "[name]"},
				{enabled = true, width = 1, name = L["Text"], text = "[warlock:demonic:curpp]", anchorTo = "$demonicFuryBar", anchorPoint = "C", x = 3, y = 0, size = -1, default = true},
				{enabled = true, width = 1, name = L["Text"], text = "[druid:eclipse]", anchorTo = "$eclipseBar", anchorPoint = "CLI", size = 0, x = 0, y = 0, default = true},
				{enabled = true, width = 1, name = L["Timer Text"], text = "[totem:timer]", anchorTo = "$totemBar", anchorPoint = "C", x = 0, y = 0, size = 0, default = true, block = true},
				{enabled = true, width = 1, name = L["Timer Text"], text = "[rune:timer]", anchorTo = "$runeBar", anchorPoint = "C", size = 0, x = 0, y = 0, default = true, block = true},
				{enabled = true, width = 1, name = L["Text"], text = "[monk:abs:stagger]", anchorTo = "$staggerBar", anchorPoint = "C", size = 0, x = 0, y = 0, default = true}
			},
		},
		target = {
			width = 300,
			height = 80,
			scale = 1.0,
			portrait = {enabled = false, alignment = "RIGHT", fullAfter = 50},

			auras = {
				buffs = {enabled = false},
				debuffs = {enabled = false},
			},
			text = {
				{text = "[name]"},
				{text = "[curmaxhp]"},
				{text = "[level( )][classification( )][perpp]", width = 0.50},
				{text = "[curmaxpp]", anchorTo = "$powerBar", width = 0.60},
				{text = "[name]"},
			},
		},

	}
	
	finalizeData(config, useMerge)
end
	
